package com.EasePark.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name="vehicle")
public class Vehicle {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int vehicleId;
	private String name;
	private String number_Plate;
	@ManyToOne
	@JoinColumn(name = "userId")
	private User user;
	public int getVehicle_Id() {
		return vehicleId;
	}
	public void setVehicle_Id(int vehicle_Id) {
		this.vehicleId = vehicle_Id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNumber_Plate() {
		return number_Plate;
	}
	public void setNumber_Plate(String number_Plate) {
		this.number_Plate = number_Plate;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	
	public Vehicle() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Vehicle [vehicle_Id=" + vehicleId + ", name=" + name + ", number_Plate=" + number_Plate + ", user="
				+ user + "]";
	}
	public Vehicle(int vehicle_Id, String name, String number_Plate, User user) {
		super();
		this.vehicleId = vehicle_Id;
		this.name = name;
		this.number_Plate = number_Plate;
		this.user = user;
	}
	
	
	

	
	
	//vehicle_Id	Category	number_Plate	owner_Id	name
}
