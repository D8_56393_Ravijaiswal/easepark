package com.EasePark.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name="parking")
public class Parking {

	//ID	parking_Name	location	no_of_CarSlots	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@OneToMany(mappedBy ="parking")
	private List<Booking> bookingList;
	private int id;
	private String parking_Name;
	private String location;
	private String no_of_Slots;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getParking_Name() {
		return parking_Name;
	}
	public void setParking_Name(String parking_Name) {
		this.parking_Name = parking_Name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getNo_of_Slots() {
		return no_of_Slots;
	}
	public void setNo_of_Slots(String no_of_Slots) {
		this.no_of_Slots = no_of_Slots;
	}
	public Parking(int id, String parking_Name, String location, String no_of_Slots) {
		super();
		this.id = id;
		this.parking_Name = parking_Name;
		this.location = location;
		this.no_of_Slots = no_of_Slots;
	}
	public Parking() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Parking [id=" + id + ", parking_Name=" + parking_Name + ", location=" + location + ", no_of_Slots="
				+ no_of_Slots + "]";
	}
	
	
	
	
}
