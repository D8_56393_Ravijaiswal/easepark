package com.EasePark.entity;

//import java.sql.Timestamp;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name="booking")
@Entity
public class Booking {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int bId;
	private int slotNo;
	@ManyToOne
	@JoinColumn(name = "vId")
	private Vehicle vehicle;
	@ManyToOne
	@JoinColumn(name = "parkId")
	private Parking parking;
	private LocalDateTime entryTime;
	private LocalDateTime exitTime;
	public int getbId() {
		return bId;
	}
	public void setbId(int bId) {
		this.bId = bId;
	}
	public int getSlotNo() {
		return slotNo;
	}
	public void setSlotNo(int slotNo) {
		this.slotNo = slotNo;
	}
	public Vehicle getVehicle() {
		return vehicle;
	}
	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	public Parking getParking() {
		return parking;
	}
	public void setParking(Parking parking) {
		this.parking = parking;
	}
	public LocalDateTime getEntryTime() {
		return entryTime;
	}
	public void setEntryTime(LocalDateTime entryTime) {
		this.entryTime = entryTime;
	}
	public LocalDateTime getExitTime() {
		return exitTime;
	}
	public void setExitTime(LocalDateTime exitTime) {
		this.exitTime = exitTime;
	}
	public Booking(int bId, int slotNo, Vehicle vehicle, Parking parking, LocalDateTime entryTime,
			LocalDateTime exitTime) {
		super();
		this.bId = bId;
		this.slotNo = slotNo;
		this.vehicle = vehicle;
		this.parking = parking;
		this.entryTime = entryTime;
		this.exitTime = exitTime;
	}
	@Override
	public String toString() {
		return "Booking [bId=" + bId + ", slotNo=" + slotNo + ", vehicle=" + vehicle + ", parking=" + parking
				+ ", entryTime=" + entryTime + ", exitTime=" + exitTime + "]";
	}
	
	
	

	
	
	
 
}
