package com.EasePark;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EaseParkApplication {

	public static void main(String[] args) {
		SpringApplication.run(EaseParkApplication.class, args);
	}

}
