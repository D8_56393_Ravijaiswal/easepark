package com.EasePark.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.EasePark.entity.User;

public interface UserDao extends JpaRepository<User, Integer> {
	
	User findBYEmail(String email);
	
	

}
