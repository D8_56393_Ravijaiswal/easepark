package com.EasePark.service;

import javax.transaction.Transactional;
import javax.websocket.server.ServerEndpoint;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.stereotype.Service;

import com.EasePark.dao.UserDao;
import com.EasePark.entity.User;

@Service
@Transactional
public class UserService {

	@Autowired
	UserDao userdao;
	
	public User findByEmail(String email)
	{
		return userdao.findBYEmail(email);	
	}
	
	public User authenticate(String email,String password)
	{	
	     User u=userdao.findBYEmail(email);
	     if(u!=null && u.getPassword().equals(password))
	     return u;
	     return null;	    	 
	}
}
