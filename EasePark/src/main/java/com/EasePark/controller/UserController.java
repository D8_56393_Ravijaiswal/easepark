package com.EasePark.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.EasePark.entity.User;
import com.EasePark.pojo.Credential;
import com.EasePark.service.UserService;

@RestController

public class UserController {
	
	@Autowired
	UserService userService;
	@PostMapping("/login")
	public ResponseEntity<?> signIn(@RequestBody Credential cred) {
		//Object userService;
		User userDto =userService.authenticate(cred.getEmail(), cred.getPassword());
		if(userDto == null)
			return Response.error("user not found");
		return Response.success(userDto);
	}
	
	

}
